package Configuracion;

/**
 * Created by jorgemanzanares on 30/04/16.
 */
public class Configuracion {

    public static float anchoAbaco = 0.58f;
    public static float margenLineaAbaco = 0.05f;

    public static float anchoLinea = 0.9f;
    public static float altoLinea = 0.02f;
    public static float margenEntreLineas = 0.25f;
    public static int numLineas = 4;

    public static float bolaWidht = 3.5f; //del tamaño de la linea.
    public static float bolaHeight = 3.5f; //del tamaño de la linea.
    public static float velocidadBola= 0.5f;

    public static float velocidadAlpha = 2f;
}

