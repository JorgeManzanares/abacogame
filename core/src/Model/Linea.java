package Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.CargarDatos;
import com.mygdx.game.MyGdxGame;


import java.util.ArrayList;

import Configuracion.Configuracion;

/**
 * Created by jorgemanzanares on 28/04/16.
 */
public class Linea {
    private Texture background;
    private Double multiplicador;
    private int position;
    public float x, y;
    public static float w, h;
    private ArrayList<Bola> bolas;

    public Linea(int posicionLinea) {
        this.multiplicador = Math.pow(10, Configuracion.numLineas - posicionLinea - 1);
        this.position = posicionLinea;
        this.background = CargarDatos.lineaTexture;
        this.x = Configuracion.margenLineaAbaco * Abaco.w;
        this.y = (Abaco.h / Configuracion.numLineas) * (position) + Abaco.h / Configuracion.numLineas / (Configuracion.numLineas / 2);
        w = (Abaco.w * Configuracion.anchoLinea);
        h = MyGdxGame.height * Configuracion.altoLinea;
        this.bolas = new ArrayList<Bola>();
        for (int i = 0; i < 10; i++) {
            bolas.add(new Bola(i, posicionLinea, multiplicador));
        }
    }

    public void draw(SpriteBatch batch) {
        batch.draw(background, x, y, w, h);
        for (Bola b : bolas) {
            b.draw(batch);
        }
    }

    public Bola getBola(int i) {
        return bolas.get(i);
    }

    public ArrayList<Bola> getBolas() {
        return bolas;
    }

    public void mover(int i) {
        if (bolas.get(i).getEstado() == 2) {
            for (int j = i; j >= 0; j--) {
                if (bolas.get(j).getEstado() != 0) {
                    bolas.get(j).setEstado(1);
                    bolas.get(j).setBolaTexture(CargarDatos.bolaMovimiento);
                }
            }
        } else if (bolas.get(i).getEstado() == 0) {
            for (int j = i; j < bolas.size(); j++) {
                if (bolas.get(j).getEstado() != 2) {
                    bolas.get(j).setEstado(1);
                    bolas.get(j).setBolaTexture(CargarDatos.bolaMovimiento);
                }
            }
        }

    }
    public boolean isAllRight(){
        if(bolas.get(0).getEstado() == 2)
            return true;
        return false;
    }

    public void corregirPosicionBolas(){
        for(Bola b: bolas){
            b.setEstado(0);
        }
    }
    public void moverPrimeraDerecha(){
        for(int i = bolas.size()-1;i>=0;i--){
            if(bolas.get(i).getEstado()==0) {
                bolas.get(i).setEstado(2);
                return;
            }
        }
    }
}