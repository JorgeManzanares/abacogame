package Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.CargarDatos;
import com.mygdx.game.MyGdxGame;

import Configuracion.Configuracion;

/**
 * Created by jorgemanzanares on 28/04/16.
 */
public class Bola implements IBola{
    private Texture bolaTexture;
    private float bolaWidth, bolaHeight;
    private float max, min;
    public static float velocity, time, space;
    private float posX, posY;
    private int estado;
    private double valor;
    private float ppr;
    private int posLinea;

    public Bola(int posicionBola, int posLinea, double multiplicador) {
        this.posLinea = posLinea;

        bolaWidth = MyGdxGame.height * Configuracion.altoLinea * Configuracion.bolaWidht;
        bolaHeight = MyGdxGame.height * Configuracion.altoLinea * Configuracion.bolaHeight;
        this.posX = Abaco.w * Configuracion.margenLineaAbaco + (posicionBola * bolaWidth);
        this.posY = ((MyGdxGame.height * Configuracion.margenEntreLineas) * (posLinea) + (MyGdxGame.height * Configuracion.margenEntreLineas / 2)) + Linea.h / 2 - bolaHeight / 2;
        this.max = Linea.w + Configuracion.margenLineaAbaco * Abaco.w - (bolaWidth * (10 - posicionBola));
        this.min = Configuracion.margenLineaAbaco * Abaco.w + bolaWidth * posicionBola;
        this.estado = 0;
        this.valor = multiplicador;
        this.time = Configuracion.velocidadBola;
        this.space = max - min;
        this.velocity = space / time;
        correctTexture(posLinea);
    }

    private void correctTexture(int posLinea) {
        switch (posLinea) {
            case 0:
                this.bolaTexture = CargarDatos.bolaTextureUnidad;
                break;
            case 1:
                this.bolaTexture = CargarDatos.bolaTextureDecena;
                break;
            case 2:
                this.bolaTexture = CargarDatos.bolaTextureCentena;
                break;
            case 3:
                this.bolaTexture = CargarDatos.bolaTextureMillar;
                break;
        }
    }

    public void draw(SpriteBatch batch) {
        movimiento();
        batch.draw(bolaTexture, posX, posY, bolaWidth, bolaHeight);
    }

    public void move(float x, float y) {
        if (estado != 1)
            return;
        posX = MathUtils.clamp(posX + x, min, max);
        posY += y;
    }

    public void calcularDestino() {
        if (posX > ((max - min) * 0.5) + min) {
            estado = 2;
        } else {
            estado = 0;
        }
        correctTexture(posLinea);
    }

    public void movimiento() {
        if (estado == 2 && posX != max) {
            ppr = velocity * Gdx.graphics.getDeltaTime();
            posX = MathUtils.clamp(posX + ppr, min, max);
        }
        if (estado == 0 && posX != min) {
            ppr = velocity * Gdx.graphics.getDeltaTime();
            posX = MathUtils.clamp(posX - ppr, min, max);
        }
    }

    public boolean isTouched(float x, float yy) {
        float y = MyGdxGame.height - yy;
        if (x > posX && x < posX + bolaWidth && y > posY && y < posY + bolaHeight)
            return true;
        return false;
    }

    public boolean isTouched(float x) {
        if (x > posX && x < posX + bolaWidth)
            return true;
        return false;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Double getValor() {
        return this.valor;
    }

    public void initPosicion() {
        posX = min;
        estado = 0;
    }

    public void setBolaTexture(Texture bolaTexture) {
        this.bolaTexture = bolaTexture;
    }
}
