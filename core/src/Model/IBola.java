package Model;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Jorge on 19/09/2016.
 */

public interface IBola {
    void draw(SpriteBatch batch);
    void move(float x, float y);
    void calcularDestino();
    boolean isTouched(float x, float y);
    boolean isTouched(float x);
    void movimiento();
}
