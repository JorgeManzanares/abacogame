package Model;

/**
 * Created by Jorge on 19/09/2016.
 */

public interface IAbaco {
    void corregirPosicionBolas();
    int calcularPuntuacion();
}
