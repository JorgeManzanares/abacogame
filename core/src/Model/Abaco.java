package Model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.CargarDatos;

import java.util.ArrayList;

/**
 * Created by jorgemanzanares on 28/04/16.
 */

public class Abaco implements IAbaco {
    Texture background;
    int puntuacion;
    static float x, y, w, h;
    ArrayList<Linea> lines;

    public Abaco(float x, float y, float w, float h, int lines) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.lines = new ArrayList<Linea>();
        for (int i = 0; i < lines; i++) {
            this.lines.add(new Linea(i));
        }
        this.puntuacion = 0;
        background = CargarDatos.abacoTexture;

    }

    public void draw(SpriteBatch batch) {
        batch.draw(background, x, y, w, h);
        for (Linea l : lines) {
            l.draw(batch);
        }
    }

    public Linea getLine(int i) {
        return lines.get(i);
    }

    public ArrayList<Linea> getLines() {
        return lines;
    }

    public int calcularPuntuacion() {
        puntuacion = 0;
        for (Linea l : lines) {
            for (Bola b : l.getBolas()) {
                if (b.getEstado() == 2)
                    puntuacion += b.getValor();
            }
        }
        return puntuacion;
    }

    public float getWidth() {
        return this.w;
    }

    public void init() {
        for (Linea l : lines) {
            for (Bola b : l.getBolas()) {
                b.initPosicion();
            }
        }
    }

    public void corregirPosicionBolas() {
        for (int i = 3; i >= 1; i--) {
            if (lines.get(i).isAllRight() && !lines.get(i - 1).isAllRight()) {
                lines.get(i).corregirPosicionBolas();
                lines.get(i - 1).moverPrimeraDerecha();
            }
        }
    }
}
