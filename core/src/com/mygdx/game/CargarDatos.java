package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

/**
 * Created by jorgemanzanares on 28/04/16.
 */
public class CargarDatos {
    public static Texture lineaTexture,bolaTextureDecena, abacoTexture, bolaTextureUnidad,
            bolaTextureCentena, bolaTextureMillar, bolaMovimiento, badlogic;
    public static void loadTextures(){
        lineaTexture = new Texture("white.png");
        badlogic = new Texture("badlogic.jpg");
        bolaMovimiento = new Texture("bolaMovimiento.png");
        bolaTextureUnidad = new Texture("bolaUnidad.png");
        bolaTextureDecena = new Texture("bolaDecena.png");
        bolaTextureCentena = new Texture("bolaCentena.png");
        bolaTextureMillar = new Texture("bolaMillar.png");
        abacoTexture = new Texture("abaco.jpg");
    }
}
