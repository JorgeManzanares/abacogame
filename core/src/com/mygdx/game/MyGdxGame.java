package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Pantallas.*;

public class MyGdxGame extends ApplicationAdapter {
    public static Pantallas pantalla;
    public static SpriteBatch batch;
    public static PantallaJuego pJuego;
    public static PantallaInicio pInicio;
    public static PopUp pPopUp;
	public static int height, width;

    @Override
    public void create() {
        height =Gdx.graphics.getHeight();
        width=Gdx.graphics.getWidth();
        CargarDatos.loadTextures();
        pJuego = new PantallaJuego();
        pJuego.load();
        pInicio = new PantallaInicio();
        pInicio.load();
        pPopUp = new PopUp();
        pPopUp.load();
		batch = new SpriteBatch();
        pInicio.init();
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glEnable(GL20.GL_BLEND);
        batch.begin();
        switch (pantalla){
            case P_INICIO:
                pInicio.draw(batch);
                break;
            case P_JUEGO:
                pJuego.draw(batch);
                break;
            case P_POPUP:
                pPopUp.draw(batch);
            default:
                break;
        }
        batch.end();
	}

    public static void changeState() {
        switch(pantalla){
            case P_INICIO:
                Gdx.app.exit();
                break;
            case P_JUEGO:
                pInicio.init();
                break;
            case P_POPUP:
                pJuego.init();
                break;
        }
    }
}