package Pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.MyGdxGame;

/**
 * Created by jorgemanzanares on 01/05/16.
 */
public class PantallaInicio implements IPantalla {
    BitmapFont font12;
    FreeTypeFontGenerator generator;
    FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    GlyphLayout glyphLayout;
    float gx, gy;
    public PantallaInicio(){}

    @Override
    public void load() {
        generator = new FreeTypeFontGenerator(Gdx.files.internal("Chunkfive.otf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 75;
        glyphLayout = new GlyphLayout();
        font12 = generator.generateFont(parameter);
        font12.setColor(1,1,1,1);
        glyphLayout.setText(font12, "Play");
        gx=MyGdxGame.width/2-glyphLayout.width/2;
        gy=MyGdxGame.height*0.4f;

    }

    @Override
    public void draw(SpriteBatch batch) {
        font12.draw(batch,glyphLayout, gx, gy);
    }

    @Override
    public void init() {
        MyGdxGame.pantalla = Pantallas.P_INICIO;
        Gdx.input.setInputProcessor(new GestureDetector(20, 0.5f, 2, 0.15f, new GestureDetector.GestureListener() {
            @Override
            public boolean touchDown(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean tap(float x, float y, int count, int button) {
                if(x>gx && x<gx+glyphLayout.width &&
                   y>gy && y<MyGdxGame.height-gy+glyphLayout.height)
                    MyGdxGame.pJuego.init();
                return false;
            }

            @Override
            public boolean longPress(float x, float y) {
                return false;
            }

            @Override
            public boolean fling(float velocityX, float velocityY, int button) {
                return false;
            }

            @Override
            public boolean pan(float x, float y, float deltaX, float deltaY) {
//
                return false;
            }

            @Override
            public boolean panStop(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean zoom(float initialDistance, float distance) {
                return false;
            }

            @Override
            public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
                return false;
            }

        }));
    }
}
