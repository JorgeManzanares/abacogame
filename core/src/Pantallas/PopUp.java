package Pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.CargarDatos;
import com.mygdx.game.MyGdxGame;

import Configuracion.Configuracion;

/**
 * Created by jorgemanzanares on 05/05/16.
 */
public class PopUp implements IPantalla {
    float alpha;
    float ppr;
    //Text Completed
    BitmapFont font;
    FreeTypeFontGenerator generator;
    FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    GlyphLayout levelCompleted;
    Texture texture;
    Sprite textCompleted;
    //Button Continue
    BitmapFont fontContinue;
    GlyphLayout buttonContinue;
    float continueX, continueY;
    //Button Cancel
    BitmapFont fontMenu;
    GlyphLayout glyphMenu;

    int margin;
    public PopUp(){}

    @Override
    public void load() {
        margin = 100;
        alpha = 1;
        texture = new Texture("white.png");
        textCompleted = new Sprite(texture, texture.getWidth(), texture.getHeight());
        levelCompleted = new GlyphLayout();
        buttonContinue = new GlyphLayout();
        glyphMenu = new GlyphLayout();
        generator = new FreeTypeFontGenerator(Gdx.files.internal("Chunkfive.otf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.color.a = 0;
        parameter.size = 150;
        parameter.color.r = 255;
        parameter.color.g = 100;
        parameter.color.b = 50;
        font = generator.generateFont(parameter);
        parameter.size = 75;
        parameter.color.r = 1;
        parameter.color.g = 0;
        parameter.color.b = 0;
        fontContinue = generator.generateFont(parameter);

        fontMenu = generator.generateFont(parameter);
    }
    @Override
    public void draw(SpriteBatch batch){ //TODO: RECOLOCAR TODA LA PANTALLA, ESTA MAL!!!!!
        batch.end();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.enableBlending();
        movimiento();
        batch.begin();
        //batch.setColor(0, 0, 0, alpha);
        textCompleted.draw(batch);
//        batch.draw(texture,
//                Gdx.graphics.getWidth() / 2 - levelCompleted.width / 2 - margin / 2,
//                Gdx.graphics.getHeight() / 2 + levelCompleted.height / 2 - margin / 2,
//                levelCompleted.width + margin,
//                levelCompleted.height + margin);

        batch.draw(CargarDatos.bolaTextureUnidad,
                continueX - margin / 2,
                continueY - buttonContinue.height - margin / 2,
                buttonContinue.width + margin,
                buttonContinue.height + margin);

        font.setColor(font.getColor().r, font.getColor().g, font.getColor().b, alpha);
        fontContinue.setColor(fontContinue.getColor().r, fontContinue.getColor().g, fontContinue.getColor().b, alpha);

        font.draw(batch,
                levelCompleted,
                Gdx.graphics.getWidth() / 2 - levelCompleted.width / 2,
                Gdx.graphics.getHeight() / 2 + levelCompleted.height / 2 + levelCompleted.height - 70);

        fontContinue.draw(batch,
                buttonContinue,
                continueX,
                continueY);

    }

    @Override
    public void init() {
        MyGdxGame.pantalla = Pantallas.P_POPUP;
        ppr = 0;
        alpha = 0;
        levelCompleted.setText(font, "      Level\nCompleted");
        buttonContinue.setText(fontContinue, "Continue");
        glyphMenu.setText(fontMenu, "Cancel");
        continueY = Gdx.graphics.getHeight() / 5 + buttonContinue.height / 2 + buttonContinue.height - 80;
        continueX = Gdx.graphics.getWidth() / 2 + buttonContinue.width;
        Gdx.input.setInputProcessor(new GestureDetector(20, 0.5f, 2, 0.15f, new GestureDetector.GestureListener() {
            @Override
            public boolean touchDown(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean tap(float x, float y, int count, int button) {
                if(x>continueX && x<continueX + buttonContinue.width &&
                        y>continueY && y<MyGdxGame.height - continueY+buttonContinue.height) {
                    MyGdxGame.pJuego.init();
                }
                return false;
            }

            @Override
            public boolean longPress(float x, float y) {
                return false;
            }

            @Override
            public boolean fling(float velocityX, float velocityY, int button) {
                return false;
            }

            @Override
            public boolean pan(float x, float y, float deltaX, float deltaY) {
                return false;
            }

            @Override
            public boolean panStop(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean zoom(float initialDistance, float distance) {
                return false;
            }

            @Override
            public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
                return false;
            }

        }));
    }

    private void movimiento(){
        ppr = 1/Configuracion.velocidadAlpha * Gdx.graphics.getDeltaTime();
        alpha = MathUtils.clamp(alpha - ppr, 0, 1);
    }
}
