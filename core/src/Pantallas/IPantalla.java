package Pantallas;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by jorgemanzanares on 28/04/16.
 */
public interface IPantalla {
    void load();
    void draw(SpriteBatch batch);
    void init();
}
