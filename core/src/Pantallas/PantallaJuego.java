package Pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.MyGdxGame;
import com.sun.org.apache.bcel.internal.generic.POP;

import java.util.ArrayList;

import Configuracion.Configuracion;
import Model.Abaco;
import Model.Bola;

/**
 * Created by jorgemanzanares on 28/04/16.
 */
public class PantallaJuego implements Pantallas.IPantalla {
    BitmapFont font, font12;
    private Abaco abaco;
    private int lineaPulsada, bolaPulsada;
    FreeTypeFontGenerator generator;
    FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    GlyphLayout glyphLayout, glyphLayoutResultado;
    int puntuacion;
    int resultado;
    ArrayList<Bola>bolasMov;
    int estado;
    PopUp pop;
    private boolean actuallyTouched=true;

    public PantallaJuego(){}
    @Override
    public void load() {
        glyphLayoutResultado = new GlyphLayout();
        glyphLayout = new GlyphLayout();
        generator = new FreeTypeFontGenerator(Gdx.files.internal("Chunkfive.otf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 150;
        lineaPulsada = -1;
        puntuacion = 0;
        estado = 0;
        pop = new PopUp();
        bolasMov = new ArrayList<Bola>();
        font12 = generator.generateFont(parameter);
        abaco = new Abaco(0, 0, (MyGdxGame.width*Configuracion.anchoAbaco)-Configuracion.margenLineaAbaco, MyGdxGame.height, Configuracion.numLineas);
        parameter.size= 50;
        font = generator.generateFont(parameter);
    }

    @Override
    public void draw(SpriteBatch batch) {
        abaco.draw(batch);
        batch.setColor(1, 1, 1, 1);
        font12.draw(batch, glyphLayout, (abaco.getWidth() + (MyGdxGame.width - abaco.getWidth()) / 2) - glyphLayout.width / 2, MyGdxGame.height / 2 + (parameter.size / 2));
        font.draw(batch, glyphLayoutResultado, (abaco.getWidth() + (MyGdxGame.width - abaco.getWidth()) / 2) - glyphLayoutResultado.width / 2, MyGdxGame.height * 0.8f);
        abaco.corregirPosicionBolas();
    }

    @Override
    public void init() {
        MyGdxGame.pantalla = MyGdxGame.Pantallas.pJuego;
        resultado= (int)(Math.random() * 10000 + 1);
        abaco.init();
        glyphLayout.setText(font12, abaco.calcularPuntuacion() + "");
        glyphLayoutResultado.setText(font, "" + resultado);
        Gdx.input.setInputProcessor(new GestureDetector(20, 0.5f, 2, 0.15f, new GestureDetector.GestureListener() {
            @Override
            public boolean touchDown(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean tap(float x, float y, int count, int button) {
                return false;
            }

            @Override
            public boolean longPress(float x, float y) {
                return false;
            }

            @Override
            public boolean fling(float velocityX, float velocityY, int button) {
                return false;
            }

            @Override
            public boolean pan(float x, float y, float deltaX, float deltaY) {
                if (actuallyTouched) {
                    for (int iLinea = 0; iLinea < abaco.getLines().size(); iLinea++) {
                        for (int iBola = 0; iBola < abaco.getLine(iLinea).getBolas().size(); iBola++) {
                            if (abaco.getLine(iLinea).getBola(iBola).isTouched(x, y)) {
                                for (int j = 0; j < abaco.getLine(iLinea).getBolas().size(); j++) {
                                    bolasMov.add(abaco.getLine(iLinea).getBola(j));
                                }
                                bolaPulsada = iBola;
                                lineaPulsada = iLinea;
                                actuallyTouched = false;
                                break;
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < bolasMov.size(); i++) {
                        if (abaco.getLine(lineaPulsada).getBola(bolaPulsada).isTouched(x)) {
                            abaco.getLine(lineaPulsada).mover(bolaPulsada);
                            for (int j = 0; j < abaco.getLine(lineaPulsada).getBolas().size(); j++) {
                                abaco.getLine(lineaPulsada).getBola(j).move(deltaX, 0);
                            }
                        }
                    }
                }
                return false;
            }

            @Override
            public boolean panStop(float x, float y, int pointer, int button) {
                if (lineaPulsada == -1) return false;
                for (int iBola = 0; iBola < abaco.getLine(lineaPulsada).getBolas().size(); iBola++) {
                    abaco.getLine(lineaPulsada).getBola(iBola).calcularDestino();
                }
                puntuacion = abaco.calcularPuntuacion();
                glyphLayout.setText(font12, abaco.calcularPuntuacion() + "");
                if (puntuacion == resultado) {
                    MyGdxGame.pPopUp.init();
                    puntuacion = 0;
                }
                actuallyTouched = true;
                return false;
            }

            @Override
            public boolean zoom(float initialDistance, float distance) {
                return false;
            }

            @Override
            public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
                return false;
            }

        }));
    }
}